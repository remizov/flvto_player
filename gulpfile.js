var gulp = require('gulp'),
    minify = require('gulp-minify'),
    cleanCSS = require('gulp-clean-css'),
    concat = require('gulp-concat');

gulp.task('default', function() {
    gulp.src(['node_modules/video.js/dist/video-js/video.js','src/scripts/*.js'])
        .pipe(concat('video-advast.js'))
        .pipe(minify({
            exclude: ['tasks'],
            ignoreFiles: ['.combo.js', '-min.js']
        }))
        .pipe(gulp.dest('dist/scripts/'));

    gulp.src([
        'node_modules/video.js/dist/video-js/video-js.min.css',
        'src/styles/*.css'
    ])
        .pipe(concat('video-advast.css'))
        .pipe(cleanCSS({debug: false}))
        .pipe(gulp.dest('dist/styles'));

    gulp.src('node_modules/video.js/dist/video-js/font/*.*')
        .pipe(gulp.dest('dist/styles/font'));
});
